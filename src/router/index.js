import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
// import AboutView from '../views/AboutView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL), // base url: http://rec-vueproject.com/page/customer/1245/setting -> home
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    }, {
      path: '/about',
      name: 'about',
      component: () => import('../views/AboutView.vue')
    }
  ]
})

export default router
